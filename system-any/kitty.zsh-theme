if [ $UID -eq 0 ]; then NCOLOR="green"; else NCOLOR="white"; fi

user='%{$fg[magenta]%}%B%n%b%{$reset_color%}'
host='@$fg[magenta]%}%B%m%b%{$reset_color%}:%{$fg[magenta]%}%30<...<%~%<<%{$reset_color%}'
PROMPT='['$user$host']%(!.#.$) '
RPROMPT='$(git_prompt_info)'

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg_bold[gray]%}(%{$fg_no_bold[yellow]%}%B"
ZSH_THEME_GIT_PROMPT_SUFFIX="%b%{$fg_bold[gray]%})%{$reset_color%} "
ZSH_THEME_GIT_PROMPT_CLEAN=""
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg_bold[red]%}(x)"

export LSCOLORS="Gxfxcxdxbxegedabagacad"
export LS_COLORS="di=34:ln=35:so=32:pi=33:ex=31:bd=34;46:cd=34;43:su=30;41:sg=30;46:tw=30;42:ow=31;40"
