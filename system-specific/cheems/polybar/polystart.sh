#!/bin/bash
xrandr --query | grep ' connected' | cut -d' ' -f1 | while read m; do
MONITOR=$m polybar -r bar &
sleep 1
done
