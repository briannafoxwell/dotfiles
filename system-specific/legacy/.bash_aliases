alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias sr='screen -r' # im lazy
alias ss='screen -S'
alias sls='screen -ls'
sk() {
    screen -X -S "$1" kill
}